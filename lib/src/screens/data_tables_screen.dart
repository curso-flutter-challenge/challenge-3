import 'package:flutter/material.dart';

class DataTablesScreen extends StatefulWidget {
  @override
  _DataTablesScreenState createState() => _DataTablesScreenState();
}

class _DataTablesScreenState extends State<DataTablesScreen> {
  final List<Map<String, dynamic>> datos = [
    {'nombre': 'Joel', 'edad': 30, 'email': 'joel@gmail.com'},
    {'nombre': 'Martha', 'edad': 25, 'email': 'martha@gmail.com'},
    {'nombre': 'Celia', 'edad': 40, 'email': 'celia@gmail.com'},
    {'nombre': 'Enrique', 'edad': 8, 'email': 'enrique@gmail.com'},
    {'nombre': 'Luz', 'edad': 4, 'email': 'luz@gmail.com'},
    {'nombre': 'Mery', 'edad': 12, 'email': 'mery@gmail.com'},
  ];

  int ordenColumna = 0;
  bool ascendente = true;

  void sortByColumn(int columna, bool ascendente) {
    setState(() {
      ordenColumna = columna;
      ascendente = ascendente;

      datos.sort((a, b) {
        if (ascendente) {
          return a.values
              .toList()[columna]
              .compareTo(b.values.toList()[columna]);
        } else {
          return b.values
              .toList()[columna]
              .compareTo(a.values.toList()[columna]);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          sortAscending: ascendente,
          sortColumnIndex: ordenColumna,
          columns: [
            DataColumn(
              label: const Text('Nombre'),
              onSort: (columna, ascendente) {
                sortByColumn(columna, ascendente);
              },
            ),
            DataColumn(
              label: const Text('Edad'),
              numeric: true,
              onSort: (columna, ascendente) {
                sortByColumn(columna, ascendente);
              },
            ),
            DataColumn(
              label: const Text('Email'),
              onSort: (columna, ascendente) {
                sortByColumn(columna, ascendente);
              },
            ),
          ],
          rows: datos
              .map((dato) => DataRow(
                    cells: [
                      DataCell(Text(dato['nombre'])),
                      DataCell(Text(dato['edad'].toString())),
                      DataCell(Text(dato['email'])),
                    ],
                  ))
              .toList(),
        ),
      ),
    );
  }
}
